create table departments
(
    id             serial       not null,
    name           varchar(100) not null,
    institute_id   int          not null,
    subjects_count int,
    groups_count   int,
    created_date   timestamp    not null,
    updated_date   timestamp    null,
    removed_date   timestamp    null,
    primary key (id),
    foreign key (institute_id) references institutes (id)
);

alter table groups
    add foreign key (department_id) references departments (id);