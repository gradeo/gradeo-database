create table teacher
(
    id            serial not null,
    name          varchar(100),
    position      varchar(50),
    department_id int,
    created_date  timestamp without time zone,
    removed_date  timestamp without time zone,
    updated_date  timestamp without time zone,
    primary key (id)
);