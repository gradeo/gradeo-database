create table tasks
(
    id           serial       not null,
    name         varchar(100) not null,
    type         varchar(50)  not null,
    subject_id   int          not null,
    created_date timestamp    not null,
    updated_date timestamp    null,
    removed_date timestamp    null,
    primary key (id),
    foreign key (subject_id) references subjects (id)
);